import Vue from 'vue';
import Router from 'vue-router';

import Dashboard from '../components/dashboard/dashboard'
import TestCase from '../components/new-testcase/new-testcase'
import Tag from '../components/tag/tag'
import Project from '../components/project/project'
import Command from '../components/command/form/form'
import CommandActions from '../components/command/form/command-action'
import commandTable from '../components/command/table/command-table'
import ScriptTable from '../components/custom_script/table/custom_script-table'
import ScriptForm from '../components/custom_script/form/custom_script'
import DefaultParams from '../components/default_params/default_params'
Vue.use(Router);

export default new Router({
    routes: [{
            path: '/',
            component: Dashboard,
        },
        {
            path: '/testcases',
            component: TestCase,
        },
        {
            path: '/testcases/:id',
            component: TestCase,
        },
        {
            path: '/tags',
            component: Tag
        },
        {
            path: '/projects',
            component: Project
        },
        {
            path: '/command-table',
            component: commandTable
        },
        {
            path: '/command',
            component: Command,
        },
        {
            path: '/command/:id',
            component: Command,
        },
        {
            path: '/test',
            component: CommandActions,
        },
        {
            path: '/scripts-table',
            component: ScriptTable
        },
        {
            path: '/script',
            component: ScriptForm,
        },
        {
            path: '/script/:id',
            component: ScriptForm,
        },
        {
            path: '/default-params',
            component: DefaultParams
        }
    ]
});