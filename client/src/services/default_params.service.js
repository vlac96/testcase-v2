import axios from 'axios';

export const http = {
  async save(data) {
    const response = await axios.post('/default_params/save', data, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },
  async delete(ids) {
    const response = await axios.post('/default_params/delete', {ids: ids}, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },
  async getAll(query) {
    const response = await axios.post('/default_params/all', query, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },
  async exportDefaultParams() {
    await axios.post('/default_params/download', {}, {
      'Content-Type': 'application/json'
    }).then(() => {
      window.open('/default_params/download');
    }).catch(() => {
      window.alert('Download Error');
    });
  },
  async uploadFile (data) {
    const formData = new FormData();
    formData.append('file', data);
    const response = await axios.post('default_params/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }}
    );

    return response.data;
  },
};