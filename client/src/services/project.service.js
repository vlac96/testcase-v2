import axios from 'axios';

export const http = {
  async save(data) {
    const response = await axios.post('/projects/save', data, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },
  async delete(id) {
    const response = await axios.post('/projects/delete', {id: id}, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },
  async getProjects(query) {
    const response = await axios.post('/projects/all', query, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },
};