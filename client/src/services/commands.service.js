import axios from 'axios';

export const http = {
  async save(data) {
    const response = await axios.post('/commands/save', data, {
      'Content-Type': 'application/json'
    });
    return response;
  },
  async delete(id) {
    const response = await axios.post('/commands/delete', {id: id}, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async getCommands(query) {
    const response = await axios.post('/commands/all', query, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async getCommand(commandId) {
    const response = await axios.post('/commands/show', {id: commandId}, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async getCombineData(commandId) {
    const response = await axios.post('/commands/combine', {id: commandId}, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async getCommandsOptions() {
    const response = await axios.get('/commands/all', {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async exportCommands(commandIds) {
    await axios.post('/commands/download', commandIds, {
      'Content-Type': 'application/json'
    }).then(() => {
      window.open('/commands/download');
    }).catch(() => {
      window.alert('Download Error');
    });
  },

  
  async uploadFile (data) {
    const formData = new FormData();
    formData.append('file', data);
    const response = await axios.post('commands/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }}
    );

    return response.data;
  },

  async deleteMulti (commandIds) {
    const response = await axios.post('/commands/delete-multi', {ids: commandIds}, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async deleteActions(commandActionsId) {
    await axios.post('/commands/delete-command-actions', {ids: commandActionsId}, {
      'Content-Type': 'application/json'
    });
  },

  async deleteCommandParamsInfo(commandParamsInfoId) {
    await axios.post('/commands/delete-command-params-info', {ids: commandParamsInfoId}, {
      'Content-Type': 'application/json'
    });
  }
};