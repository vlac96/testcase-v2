import axios from 'axios';

export const http = {
  async save(data) {
    const response = await axios.post('/tags/save', data, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },
  async delete(id) {
    const response = await axios.post('/tags/delete', {id: id}, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async getTags(query) {
    const response = await axios.post('/tags/all', query, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

};