import axios from 'axios';

export const http = {
  async save(data) {
    const response = await axios.post('/custom-scripts/save', {...data}, {
      'Content-Type': 'application/json'
    });
    return response;
  },

  async onQuery(query) {
    const response = await axios.post('/custom-scripts/all', query, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async getCustomScript(customScriptId) {
    const response = await axios.post('/custom-scripts/show', {id: customScriptId}, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async deleteMulti (commandIds) {
    const response = await axios.post('/custom-scripts/delete-multi', {ids: commandIds}, {
      'Content-Type': 'appication/json'
    });

    return response.data;
  },

  async getAll () {
    const response = await axios.get('/custom-scripts/all', {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async removeParams (scriptParamsInfoId) {
    await axios.post('/custom-scripts/delete-params', {ids: scriptParamsInfoId}, {
      'Content-Type': 'application/json'
    });

    return true;
  },

  async getScriptCombine(scriptId) {
    const response = await axios.post('/custom-scripts/combine', {id: scriptId}, {
      'Content-Type': 'application/json'
    });

    return response.data;
  }
};