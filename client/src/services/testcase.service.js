import axios from 'axios';

export const http = {
  async runTestcase(data) {
    const response = await axios.post('/testcases/run', data, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  // async stopTestcase() {
  //   await axios.post('/testcases/stop', {}, {
  //     'Content-Type': 'application/json'
  //   });
  // },

  async deleteActions(actions) {
    await axios.post('/testcases/delete-actions', {ids: actions}, {
      'Content-Type': 'application/json'
    });
  },

  async deleteOutputs(outputs) {
    await axios.post('/testcases/delete-outputs', {ids: outputs}, {
      'Content-Type': 'application/json'
    });
  },

  async getTestcase(data) {
    const response = await axios.post('/testcases/show', {
      id: data
    }, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async saveTestcase(testcase) {
    const response = await axios.post('/testcases/save', {
      data: testcase
    }, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async deleteTestcase(testcaseId) {
    const response = await axios.post('/testcases/delete', {
      id: testcaseId
    }, {
      'Content-Type': 'application/json'
    });

    return response;
  },

  async getTestCases(data) {
    const response = await axios.post('/testcases', data, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async getTestCasesOptions() {
    const response = await axios.post('/testcases/testcase-options', {}, {
      'Content-Type': 'application/json'
    });
    return response.data;
  },

  async getProjects() {
    const response = await axios.get('/projects/all', {}, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async getTags() {
    const response = await axios.get('/tags/all', {}, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async runMulti(testCasesId, keepSessionId) {
    const response = await axios.post('/testcases/run-multi', {
      keepSession: keepSessionId,
      testcases: testCasesId
    }, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async updateProject(data) {
    const response = await axios.post('/testcases/update', {
      ...data,
      type: 'project'
    }, {
      'Content-Type': 'appication/json'
    });

    return response.data;
  },

  async updateTags(data) {
    const response = await axios.post('testcases/update', {
      ...data,
      type: 'tags'
    }, {
      'Content-Type': 'application/json'
    });

    return response.data;
  },

  async exportIssue(testcaseIds) {
    await axios.post('/testcases/download', testcaseIds, {
      'Content-Type': 'application/json'
    }).then(() => {
      window.open('/testcases/download');
    }).catch(() => {
      window.alert('Sorry, have error in download');
    });
  },

  async uploadFile (data) {
    const formData = new FormData();
    formData.append('file', data);
    const response = await axios.post('testcases/upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }}
    );

    return response.data;
  },

  async clearError() {
    const response = await axios.post('/testcases/stop', {}, {
      'Content-Type': 'appication/json'
    });

    return response.data;
  },

  async deleteMulti (testcaseIds) {
    const response = await axios.post('/testcases/delete-multi', {ids: testcaseIds}, {
      'Content-Type': 'appication/json'
    });

    return response.data;
  },

  async swapTestcase (testcaseIds) {
    const response = await axios.post('/testcases/swap-testcase', {ids: testcaseIds}, {
      'Content-Type': 'application/json'
    });

    return response;
  },

  async checkCookie (isMobileMode) {
    await axios.post('/testcases/check-cookie', {is_mobile_mode: isMobileMode}, {
      'Content-Type': 'application/json'
    });
  }
};