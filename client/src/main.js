import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import VeeValidate from 'vee-validate';
import moment from 'moment';

Vue.config.productionTip = false
Vue.use(VeeValidate, {
  events: 'blur',
  inject: true,
});

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
});

Vue.filter('formatNullValue', function (value) {
  if (value) {
    return value.name;
  } else {
    return "null";
  }
});

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')


$(function () {
  $('body').tooltip({
    selector: '[data-toggle="tooltip"]'
  });
});