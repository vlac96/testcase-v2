# testcase-management


## Description



# NestJs & Vue: 
## Installation

```bash
$ npm install
$ cd front/js & npm install
```

## Create Database

Create database named '`testcases`', and config connection in `ormconfig.json`

```json
{
  "type": "mysql",
  "host": "#host#",
  "port": #port#,
  "username": "#server_name#",
  "password": "#server_password#",
  "database": "#database_name#",
  "entities": ["src/**/**.entity{.ts,.js}"],
  "synchronize": true
}
```

## Setup screenconfig.json file

If you want a correct result regarding screen size, you should revise the screen parameters in the `screenconfig.json` file.

```json
{
    "width": 1366,
    "height": 768
}
```

## Command to run
```bash
$ npm run driver:chrome
$ npm run start
$ npm run vue
```