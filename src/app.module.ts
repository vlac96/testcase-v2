import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

import { TagController } from './controllers/tags.controller';
import { CommandController } from './controllers/command.controller';
import { ProjectController } from './controllers/project.controller';
import { TestCaseController } from './controllers/testcase.controller';
import { CustomScriptController } from './controllers/customScript.controller';
import { DirectParamsController } from './controllers/direct_params.controller';

import { TagsService } from './shared/services/tags.service';
import { ClientsService } from './shared/services/client.service';
import { ProjectService } from './shared/services/project.service';
import { ActionsService } from './shared/services/actions.service';
import { ScriptsService } from './shared/services/scripts.service';
import { CommandsService } from './shared/services/commands.service';
import { TestCasesService } from './shared/services/testcases.service';
import { DirectParamsService } from './shared/services/direct_params.service';
import { CommandActionsService } from './shared/services/command_actions.service';
import { ScriptParamsInfoService } from './shared/services/script_params_info.service';
import { CommandParamsInfoService } from './shared/services/command_params_info.service';
import { ScriptParamsValueService } from './shared/services/script_params_value.service';
import { CommandParamsValueService } from './shared/services/command_params_value.services';
import { TestcaseRespectOutputsService } from './shared/services/testcase_respect_outputs.service';

import { Tags } from './entities/tags.entity';
import { Scripts } from './entities/scripts.entity';
import { Commands } from './entities/command.entity';
import { Projects } from './entities/project.entity';
import { TestCases } from './entities/testcase.entity';
import { DirectParams } from './entities/direct_params.entity';
import { CommandActions } from './entities/command_actions.entity';
import { TestcaseActions } from './entities/testcase_actions.entity';
import { ScriptParamsInfo } from './entities/script_params_info.entity';
import { CommandParamsInfo } from './entities/command_params_info.entity';
import { ScriptParamsValue } from './entities/script_params_value.entity';
import { CommandParamsValue } from './entities/command_params_value.entity';
import { TestcaseRespectOutputs } from './entities/testcase_respect_outputs.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    TypeOrmModule.forFeature(
      [
        TestCases, TestcaseActions, TestcaseRespectOutputs, Projects, Tags, CommandActions, CommandParamsInfo,
        Commands, CommandParamsValue, ScriptParamsValue, Scripts, ScriptParamsInfo,
        DirectParams,
      ],
    ),
  ],
  controllers: [
    TagController,
    AppController,
    CommandController,
    ProjectController,
    TestCaseController,
    CustomScriptController,
    DirectParamsController,
  ],
  providers: [
    AppService,
    TagsService,
    ProjectService,
    ClientsService,
    ActionsService,
    ScriptsService,
    CommandsService,
    TestCasesService,
    DirectParamsService,
    CommandActionsService,
    ScriptParamsInfoService,
    CommandParamsInfoService,
    ScriptParamsValueService,
    CommandParamsValueService,
    TestcaseRespectOutputsService,
  ],
})
export class AppModule { }
