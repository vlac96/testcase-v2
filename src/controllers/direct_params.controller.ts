import { Controller, Body, Post, Res, UseInterceptors, FileInterceptor, UploadedFile, HttpStatus, Render, Get } from '@nestjs/common';
import { DirectParamsService } from './../shared/services/direct_params.service';

@Controller('default_params')
export class DirectParamsController {
    constructor(
        private readonly directParamsService: DirectParamsService,
    ) { }

    @Get()
    @Render('index')
    root(): string {
        return;
    }

    @Post('/all')
    async getQuery(@Body() query) {
        const data = await this.directParamsService.getQuery(query);
        return { default_params: data[0], total: data[1] };
    }

    @Post('show')
    async find(@Body('id') id) {
        return await this.directParamsService.findById(id);
    }

    @Post('/save')
    async save(@Body() data, @Res() res) {
        try {
            if (!data.id) {
                const recordHaveSameName = await this.directParamsService.findByName(data.name);
                if (typeof recordHaveSameName !== 'undefined') {
                    return res.status(HttpStatus.OK).json({error: 'Name used! Please choose other'}).send();
                }
            }
            const result = await this.directParamsService.save(data);

            return res.status(HttpStatus.CREATED).json(result).send();
        } catch (e) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
        }
    }

    @Post('/delete')
    async delete(@Body('ids') identity, @Res() res) {
        try {
            const result = await this.directParamsService.delete(identity);
            return res.status(HttpStatus.OK).send();
        } catch (e) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
        }
    }

    @Get('download')
    async download(@Res() res) {
        const filePath = __dirname + './../../public/directParams.json';
        const fileName = 'directParams.json';

        return res.download(filePath, fileName);
    }

    @Post('download')
    async prepareData(@Body() data) {
        return await this.directParamsService.prepareExportDirectParams();
    }

    @Post('upload')
    @UseInterceptors(FileInterceptor('file'))
    async upload(@UploadedFile() file, @Res() res) {
        await this.directParamsService.importJson(file);

        return res.status(HttpStatus.OK).send();
    }
}
