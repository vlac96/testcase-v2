import { Controller, Body, Post, UseInterceptors, FileInterceptor, UploadedFile, Res, HttpStatus, Render, Get } from '@nestjs/common';
import { ScriptsService } from './../shared/services/scripts.service';
import { ScriptParamsInfoService } from './../shared/services/script_params_info.service';

@Controller('custom-scripts')
export class CustomScriptController {
    constructor(
        private readonly scriptsService: ScriptsService,
        private readonly scriptParamsInfoService: ScriptParamsInfoService,
    ) { }

    @Get()
    @Render('index')
    root(): string {
        return;
    }

    @Post('/all')
    async getProjects(@Body() query) {
        const data = await this.scriptsService.getQuery(query);
        return { customScript: data[0], total: data[1] };
    }

    @Get('/all')
    async getProjectsOptions() {
        return await this.scriptsService.getAll();
    }

    @Post('show')
    async find(@Body('id') id) {
        return await this.scriptsService.getById(id);
    }

    @Post('/save')
    async save(@Body() data, @Res() res) {
        const result = await this.scriptsService.save(data);

        if (result) {
            return res.status(HttpStatus.CREATED).json(result).send();
        }

        return res.status(HttpStatus.OK).send();
    }

    @Post('delete-multi')
    async deleteMulti(@Body('ids') ids, @Res() res) {
        await this.scriptsService.deleteMulti(ids);

        return res.status(HttpStatus.OK).send();
    }

    @Post('delete-params')
    async deleteParams(@Body('ids') ids, @Res() res) {
        for (const id of ids) {
            await this.scriptParamsInfoService.delete(id);
        }

        return res.status(HttpStatus.OK).send();
    }

    @Post('combine')
    async getScriptToCombine(@Body('id') id) {
        const data = await this.scriptsService.getById(id);

        const returnData = JSON.parse(JSON.stringify(data));

        for (const scriptParamInfo of returnData.script_params_info) {
            scriptParamInfo.script_params_value = [{
                id: null,
                value: scriptParamInfo.default_value,
            }];
        }

        return returnData;
    }

    @Get('download')
    async download(@Res() res) {
        const filePath = __dirname + './../../public/scripts.json';
        const fileName = 'scripts.json';

        return res.download(filePath, fileName);
    }

    @Post('download')
    async prepareData(@Body() data) {
        return await this.scriptsService.prepareExportScripts(data);
    }

    @Post('upload')
    @UseInterceptors(FileInterceptor('file'))
    async upload(@UploadedFile() file, @Res() res) {
        await this.scriptsService.importJson(file);

        return res.status(HttpStatus.OK).send();
    }
}
