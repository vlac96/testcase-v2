import { Controller, Body, Post, UseInterceptors, FileInterceptor, UploadedFile, Res, HttpStatus, Render, Get } from '@nestjs/common';
import { CommandsService } from './../shared/services/commands.service';
import { CommandActionsService } from './../shared/services/command_actions.service';
import { CommandParamsInfoService } from './../shared/services/command_params_info.service';

@Controller('commands')
export class CommandController {
    constructor(
        private readonly commandsService: CommandsService,
        private readonly commandActionsService: CommandActionsService,
        private readonly commandParamsInfoService: CommandParamsInfoService,
    ) { }

    @Get()
    @Render('index')
    root(): string {
        return;
    }

    @Post('/all')
    async getProjects(@Body() query) {
        const data = await this.commandsService.getQuery(query);
        return { commands: data[0], total: data[1] };
        // return data;
    }

    @Get('/all')
    async getProjectsOptions() {
        return await this.commandsService.getAll();
    }

    @Post('show')
    async find(@Body('id') id) {
        const data = await this.commandsService.findById(id);

        return data;
    }

    @Post('combine')
    async combineWithAction(@Body('id') id) {
        const data = await this.commandsService.findById(id);

        const returnData = JSON.parse(JSON.stringify(data));

        for (const command_action of returnData.command_actions) {
            for (const command_param_info of command_action.command_params_info) {
                let val;
                if (command_param_info.param_replace_type === 'replace-all')
                    val = command_action[command_param_info.attributeTo];
                else
                    val = command_param_info.param_default_value;

                command_param_info.command_params_value = [{
                    id: null,
                    value: val,
                }];
            }
        }

        return returnData;
    }

    @Post('/save')
    async save(@Body() data, @Res() res) {
        data.command_actions = this.commandParamsInfoService.filterCommandParamsInfo(data.command_actions);
        const result = await this.commandsService.save(data);

        if (result) {
            return res.status(HttpStatus.CREATED).json(result).send();
        }

        return res.status(HttpStatus.OK).send();
    }

    @Post('/delete')
    async delete(@Body('id') identity, @Res() res) {
        const result = await this.commandsService.delete(identity);

        if (!result) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
        }

        return res.status(HttpStatus.OK).send();
    }

    @Get('download')
    async download(@Res() res) {
        const filePath = __dirname + './../../public/commands.json';
        const fileName = 'commands.json';

        return res.download(filePath, fileName);
    }

    @Post('download')
    async prepareData(@Body() data) {
        return await this.commandsService.prepareExportCommands(data);
    }

    @Post('upload')
    @UseInterceptors(FileInterceptor('file'))
    async upload(@UploadedFile() file, @Res() res) {
        await this.commandsService.importJson(file);

        return res.status(HttpStatus.OK).send();
    }

    @Post('delete-multi')
    async deleteMulti(@Body('ids') ids, @Res() res) {
        await this.commandsService.deleteMulti(ids);

        return res.status(HttpStatus.OK).send();
    }

    @Post('delete-command-actions')
    async deleteCommandActions(@Body('ids') ids, @Res() res) {
        for (const id of ids) {
            await this.commandActionsService.delete(id);
        }

        return res.status(HttpStatus.OK).send();
    }

    @Post('delete-command-params-info')
    async deleteCommandParamsInfo(@Body('ids') ids, @Res() res) {
        for (const id of ids) {
            await this.commandParamsInfoService.delete(id);
        }

        return res.status(HttpStatus.OK).send();
    }
}
