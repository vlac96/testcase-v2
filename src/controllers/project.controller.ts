import { Controller, Body, Post, Res, HttpStatus, Render, Get } from '@nestjs/common';
import { ProjectService } from './../shared/services/project.service';

@Controller('projects')
export class ProjectController {
    constructor(
        private readonly projectService: ProjectService,
    ) { }

    @Get()
    @Render('index')
    root(): string {
        return;
    }

    @Post('/all')
    async getProjects(@Body() query) {
        const data = await this.projectService.getProjects(query);
        return { projects: data[0], total: data[1] };
    }

    @Get('/all')
    async getProjectsOptions() {
        return await this.projectService.getProjectsOptions();
    }

    @Post('show')
    async find(@Body('id') id) {
        const data = await this.projectService.findOne(id);

        return data;
    }

    @Post('/save')
    async save(@Body() data, @Res() res) {
        const result = await this.projectService.save(data);

        if (!result) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
        }

        return res.status(HttpStatus.CREATED).json(result).send();
    }

    @Post('/delete')
    async delete(@Body('id') identity, @Res() res) {
        const result = await this.projectService.delete(identity);

        if (!result) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
        }

        return res.status(HttpStatus.OK).send();
    }
}
