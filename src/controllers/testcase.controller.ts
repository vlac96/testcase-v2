import { Controller, Body, Post, UseInterceptors, FileInterceptor, UploadedFile, Res, HttpStatus, Render, Get } from '@nestjs/common';
import { TestCasesService } from './../shared/services/testcases.service';
import { ClientsService } from './../shared/services/client.service';
import { DirectParamsService } from './../shared/services/direct_params.service';
import { ActionsService } from './../shared/services/actions.service';
import { TestcaseRespectOutputsService } from './../shared/services/testcase_respect_outputs.service';

@Controller('testcases')
export class TestCaseController {
    constructor(
        private readonly testcaseService: TestCasesService,
        private readonly clientService: ClientsService,
        private readonly directParamsService: DirectParamsService,
        private readonly actionsService: ActionsService,
        private readonly outputsService: TestcaseRespectOutputsService,
    ) { }

    @Get()
    @Render('index')
    root(): string {
        return;
    }

    @Post()
    async findAll(@Body() query) {
        const data = await this.testcaseService.getQuery(query);
        return { testcases: data[0], total: data[1] };
    }

    @Post('/run')
    async run(@Body() data) {
        const dataToRun = await this.testcaseService.formatRunData(data);

        return await this.clientService.run(dataToRun, false);
    }

    @Post('/stop')
    async stop(@Res() res) {
        await this.clientService.clientKiller();

        return res.status(HttpStatus.OK).send();
    }

    @Post('show')
    async find(@Body('id') id) {
        const data = await this.testcaseService.findById(id);
        const res = await this.testcaseService.formatReturnData(data);

        return res;
    }

    @Post('/save')
    async save(@Body('data') data, @Res() res) {
        const result = await this.testcaseService.save(data);

        if (!result) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
        }

        return res.status(HttpStatus.CREATED).json(result).send();
    }

    @Post('testcase-options')
    async getTestCaseOptions(@Res() res) {
        const result = await this.testcaseService.getAll();
        return res.status(HttpStatus.OK).json(result).send();
    }

    @Post('/run-multi')
    async runMulti(@Body() data) {
        const testcase = await this.testcaseService.findById(data.testcases);

        const dataReturn = await this.testcaseService.formatReturnData(testcase);
        const dataToRun = await this.testcaseService.formatRunData(dataReturn);

        const res = await this.clientService.runMulti({
            keepSession: data.keepSession,
            testcases: dataToRun,
        });

        return res;
    }

    @Post('update')
    async update(@Body() data) {
        if (data.type === 'project') {
            return await this.testcaseService.updateProject(data);
        } else {
            return await this.testcaseService.updateTag(data);
        }
    }

    @Get('download')
    async download(@Res() res) {
        const filePath = __dirname + './../../public/testcases.json';
        const fileName = 'testcases.json';

        return res.download(filePath, fileName);
    }

    @Post('download')
    async prepareData(@Body() data) {
        return await this.testcaseService.prepareDownloadData(data);
    }

    @Post('upload')
    @UseInterceptors(FileInterceptor('file'))
    async upload(@UploadedFile() file, @Res() res) {
        await this.testcaseService.importJson(file);

        return res.status(HttpStatus.OK).send();
    }

    @Post('delete-multi')
    async deleteMulti(@Body('ids') ids, @Res() res) {
        await this.testcaseService.deleteMulti(ids);

        return res.status(HttpStatus.OK).send();
    }

    @Post('swap-testcase')
    async swapTestcase(@Body('ids') ids, @Res() res) {
        await this.testcaseService.swapOrderTestcase(ids);

        return res.status(HttpStatus.OK).send();
    }

    @Post('check-cookie')
    async checkCookie(@Body('is_mobile_mode') isMobileMode, @Res() res) {
        await this.clientService.checkCookieClient(isMobileMode);

        return res.status(HttpStatus.OK).send();
    }

    @Post('delete-actions')
    async deleteActions(@Body('ids') ids, @Res() res) {
        for (const id of ids) {
            await this.actionsService.delete(id);
        }

        return res.status(HttpStatus.OK).send();
    }

    @Post('delete-outputs')
    async deleteOutputs(@Body('ids') ids, @Res() res) {
        for (const id of ids) {
            await this.outputsService.delete(id);
        }

        return res.status(HttpStatus.OK).send();
    }
}
