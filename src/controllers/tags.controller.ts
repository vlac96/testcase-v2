import { Controller, Body, Post, Res, HttpStatus, Render, Get } from '@nestjs/common';
import { TagsService } from './../shared/services/tags.service';

@Controller('tags')
export class TagController {
    constructor(
        private readonly tagsService: TagsService,
    ) { }

    @Get()
    @Render('index')
    root(): string {
        return;
    }

    @Get('/all')
    async getTagsOption() {
        return await this.tagsService.getAll();
    }

    @Post('/all')
    async getTags(@Body() query) {
        const data = await this.tagsService.getQuery(query);
        return { tags: data[0], total: data[1] };
    }

    @Post('show')
    async find(@Body('id') id) {
        const data = await this.tagsService.findById(id);

        return data;
    }

    @Post('/save')
    async save(@Body() data, @Res() res) {
        const result = await this.tagsService.save(data);

        if (!result) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
        }

        return res.status(HttpStatus.CREATED).json(result).send();
    }

    @Post('/delete')
    async delete(@Body('id') identity, @Res() res) {
        const result = await this.tagsService.delete(identity);

        if (!result) {
            return res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
        }

        return res.status(HttpStatus.OK).send();
    }
}
