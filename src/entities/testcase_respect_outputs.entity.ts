import {
  Entity, Column, PrimaryGeneratedColumn, ManyToOne, UpdateDateColumn,
  CreateDateColumn, OneToMany,
} from 'typeorm';
import { TestCases } from './testcase.entity';
import { Scripts } from './scripts.entity';
import { ScriptParamsValue } from './script_params_value.entity';

@Entity('testcase_respect_outputs')
export class TestcaseRespectOutputs {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'text', nullable: true})
  element: string;

  @Column({ nullable: true })
  order: number;

  @Column()
  type: string;

  @Column({ type: 'text', nullable: true })
  has_value: string;

  @Column({ nullable: true, default: 0 })
  waiting_time: number;

  @Column({ nullable: true })
  operator: string;

  @Column({ nullable: true })
  is_compare_another_output: boolean;

  @Column({ nullable: true })
  actions_compared_order: number;

  @ManyToOne(type => TestCases, testcase => testcase.outputs, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  testcase: TestCases;

  @ManyToOne(type => Scripts, script => script.output, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  script: Scripts;

  @OneToMany(type => ScriptParamsValue, scriptParamsValue => scriptParamsValue.output, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  script_params_value: ScriptParamsValue[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}