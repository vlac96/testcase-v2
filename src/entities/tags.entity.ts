import {
    Entity, Column, PrimaryGeneratedColumn, ManyToMany, CreateDateColumn, UpdateDateColumn,
} from 'typeorm';
import { TestCases } from './testcase.entity';

@Entity('tags', {
    orderBy: {
        createdAt: 'DESC',
    },
})
export class Tags {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToMany(type => TestCases, testcases => testcases.tags)
    testcases: TestCases[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date;
}