import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';
import { TestCases } from './testcase.entity';
import { Scripts } from './scripts.entity';
import { ScriptParamsValue } from './script_params_value.entity';
import { Commands } from './command.entity';
import { CommandParamsValue } from './command_params_value.entity';

@Entity('testcase_actions', {
  orderBy: {
    order: 'DESC',
    id: 'ASC',
  },
})
export class TestcaseActions {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  action: string;

  @Column({ type: 'text', nullable: true})
  element: string;

  @Column({ type: 'text', nullable: true })
  value: string;

  @Column({ nullable: true })
  order: number;

  @Column({ type: 'text', nullable: true })
  elementTo: string;

  @Column({ nullable: true })
  xFrom: number;

  @Column({ nullable: true })
  yFrom: number;

  @Column({ nullable: true })
  xTo: number;

  @Column({ nullable: true })
  yTo: number;

  @Column('int', { default: 0 })
  waiting_time: number;

  @ManyToOne(type => TestCases, testcase => testcase.actions, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  testcase: TestCases;

  @ManyToOne(type => Scripts, script => script.action, { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: true })
  script: Scripts;

  @ManyToOne(type => Commands, command => command.action, { onDelete: 'CASCADE', nullable: true })
  command: Commands;

  @OneToMany(type => ScriptParamsValue, scriptParamsValue => scriptParamsValue.action, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  script_params_value: ScriptParamsValue[];

  @OneToMany(type => CommandParamsValue, commandParamsValue => commandParamsValue.action, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  command_params_value: ScriptParamsValue[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;

}