import {
  Entity, Column, PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('direct_params')
export class DirectParams {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  name: string;

  @Column({ nullable: true, length: 500 })
  describe: string;

  @Column()
  value: string;
}