import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, CreateDateColumn, JoinColumn } from 'typeorm';
import { ScriptParamsValue } from './script_params_value.entity';
import { Scripts } from './scripts.entity';

@Entity('script_params_info')
export class ScriptParamsInfo {
    @PrimaryGeneratedColumn()
    id: number;

    private _script_params_value: ScriptParamsValue;

    @Column()
    name: string;

    @Column()
    alias: string;

    @Column({nullable: true})
    default_value: string;

    @OneToMany(type => ScriptParamsValue, scriptParamsValue => scriptParamsValue.script_params_info, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    script_params_value: ScriptParamsValue[];

    @ManyToOne(type => Scripts, script => script.script_params_info, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    script: Scripts;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;
}