import { Entity, Column, PrimaryGeneratedColumn, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm';
import { TestCases } from './testcase.entity';
import { Commands } from './command.entity';
import { Scripts } from './scripts.entity';

@Entity('projects')
export class Projects {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(type => TestCases, testcase => testcase.project, { onDelete: 'SET NULL', onUpdate: 'CASCADE', nullable: true })
    testcases: TestCases[];

    @OneToMany(type => Commands, command => command.project, { onDelete: 'SET NULL', onUpdate: 'CASCADE', nullable: true })
    commands: Commands[];

    @OneToMany(type => Scripts, scripts => scripts.project, { onDelete: 'SET NULL', onUpdate: 'CASCADE', nullable: true })
    scripts: Scripts[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date;
}