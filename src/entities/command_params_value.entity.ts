import {
    Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, OneToMany,
} from 'typeorm';
import { TestcaseActions } from './testcase_actions.entity';
import { CommandParamsInfo } from './command_params_info.entity';

@Entity('command_params_value')
export class CommandParamsValue {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'text', nullable: true })
    value: string;

    @ManyToOne(type => CommandParamsInfo, commandParamsValue => commandParamsValue.command_params_value, { onDelete: 'CASCADE' })
    command_params_info: CommandParamsInfo;

    @ManyToOne(type => TestcaseActions, action => action.command_params_value, { nullable: true, onDelete: 'CASCADE' })
    action: TestcaseActions;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;
}