import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, CreateDateColumn, JoinColumn } from 'typeorm';
import { Commands } from './command.entity';
import { CommandParamsInfo } from './command_params_info.entity';

@Entity('command_actions')
export class CommandActions {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  action: string;

  @Column({ type: 'text', nullable: true})
  element: string;

  @Column({ type: 'text', nullable: true })
  value: string;

  @Column({ nullable: true })
  order: number;

  @Column({ type: 'text', nullable: true })
  elementTo: string;

  @Column({ nullable: true })
  xFrom: number;

  @Column({ nullable: true })
  yFrom: number;

  @Column({ nullable: true })
  xTo: number;

  @Column({ nullable: true })
  yTo: number;

  @Column('int', { default: 0 })
  waiting_time: number;

  @JoinColumn()
  @ManyToOne(type => Commands, command => command.command_actions, { onDelete: 'CASCADE' })
  command: Commands;

  @OneToMany(type => CommandParamsInfo, commandParamsInfo => commandParamsInfo.command_action, { nullable: true, onDelete: 'CASCADE' })
  command_params_info: CommandParamsInfo[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;

}