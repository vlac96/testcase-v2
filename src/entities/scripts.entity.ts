import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, CreateDateColumn, JoinColumn } from 'typeorm';
import { ScriptParamsInfo } from './script_params_info.entity';
import { TestcaseRespectOutputs } from './testcase_respect_outputs.entity';
import { TestcaseActions } from './testcase_actions.entity';
import { Projects } from './project.entity';

@Entity('scripts')
export class Scripts {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ type: 'text', nullable: true})
    content: string;

    @OneToMany(type => ScriptParamsInfo, scriptParamsInfo => scriptParamsInfo.script, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    script_params_info: ScriptParamsInfo[];

    @OneToMany(type => TestcaseRespectOutputs, output => output.script, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    output: TestcaseRespectOutputs[];

    @ManyToOne(type => Projects, project => project.scripts, { nullable: true, onDelete: 'SET NULL' })
    project: Projects;

    @OneToMany(type => TestcaseActions, action => action.script, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    action: TestcaseActions[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

}