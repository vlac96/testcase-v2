import {
    Entity, Column, PrimaryGeneratedColumn, JoinTable,
    JoinColumn, OneToMany, ManyToOne, ManyToMany, CreateDateColumn,
    UpdateDateColumn,
} from 'typeorm';
import { TestcaseActions } from './testcase_actions.entity';
import { TestcaseRespectOutputs } from './testcase_respect_outputs.entity';
import { Projects } from './project.entity';
import { Tags } from './tags.entity';

@Entity('testcases')

export class TestCases {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ nullable: true })
    issueLink: string;

    @Column({ nullable: true })
    order: number;

    @Column({ nullable: true, default: false })
    is_mobile_mode: boolean;

    @OneToMany(type => TestcaseActions, step => step.testcase, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    actions: TestcaseActions[];

    @OneToMany(type => TestcaseRespectOutputs, output => output.testcase, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    outputs: TestcaseRespectOutputs[];

    @ManyToOne(type => Projects, project => project.testcases, { nullable: true, onDelete: 'SET NULL' })
    @JoinColumn()
    project: Projects;

    @ManyToMany(type => Tags, tags => tags.testcases, { nullable: true })
    @JoinTable()
    tags: Tags[];

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

    @UpdateDateColumn({ type: 'timestamp' })
    updatedAt: Date;
}