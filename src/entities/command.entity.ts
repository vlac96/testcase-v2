import {
    Entity, Column, PrimaryGeneratedColumn,
    JoinColumn, OneToMany, ManyToOne, CreateDateColumn,
} from 'typeorm';
import { Projects } from './project.entity';
import { CommandActions } from './command_actions.entity';
// import { CommandParamsInfo } from './command_params_info.entity';
// import { TestcasesCommands } from './testcases_commands.entity';
import { TestcaseActions } from './testcase_actions.entity';

@Entity('commands')
export class Commands {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(type => CommandActions, commandActions => commandActions.command, { onDelete: 'CASCADE', nullable: true })
    command_actions: CommandActions[];

    // @OneToMany(type => CommandParamsInfo, commandParamsInfo => commandParamsInfo.command, { onDelete: 'CASCADE', nullable: true })
    // command_params_info: CommandParamsInfo[];

    @OneToMany(type => TestcaseActions, testcaseActions => testcaseActions.command, { onDelete: 'CASCADE', nullable: true })
    action: TestcaseActions[];

    @ManyToOne(type => Projects, project => project.testcases, { nullable: true, onDelete: 'SET NULL' })
    @JoinColumn()
    project: Projects;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;
}