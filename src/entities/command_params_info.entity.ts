import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, CreateDateColumn } from 'typeorm';
import { CommandActions } from './command_actions.entity';
// import { Commands } from './command.entity';
import { CommandParamsValue } from './command_params_value.entity';

@Entity('command_params_info')
export class CommandParamsInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  attributeTo: string;

  @Column({ nullable: true })
  param_replace_type: string;

  @Column({ nullable: true })
  param_default_value: string;

  @ManyToOne(type => CommandActions, commandActions => commandActions.command_params_info, { onDelete: 'CASCADE' })
  command_action: CommandActions;

  // @ManyToOne(type => Commands, command => command.command_params_info, { nullable: true, onDelete: 'CASCADE' })
  // command: Commands;

  @OneToMany(type => CommandParamsValue, commandParamsValue => commandParamsValue.command_params_info, { nullable: true, onDelete: 'CASCADE' })
  command_params_value: CommandParamsValue[];

  @CreateDateColumn({ type: 'timestamp' })
  createdAt: Date;
}