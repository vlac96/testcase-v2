import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, CreateDateColumn, JoinColumn } from 'typeorm';
import { ScriptParamsInfo } from './script_params_info.entity';
import { TestcaseRespectOutputs } from './testcase_respect_outputs.entity';
import { TestcaseActions } from './testcase_actions.entity';

@Entity('script_params_value')
export class ScriptParamsValue {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'text' })
    value: string;

    @ManyToOne(type => TestcaseRespectOutputs, output => output.script_params_value, { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: true })
    output: TestcaseRespectOutputs;

    @ManyToOne(type => TestcaseActions, action => action.script_params_value, { onDelete: 'CASCADE', onUpdate: 'CASCADE', nullable: true })
    action: TestcaseActions;

    @ManyToOne(type => ScriptParamsInfo, scriptParamsInfo => scriptParamsInfo.script_params_value, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
    script_params_info: ScriptParamsInfo;

    @CreateDateColumn({ type: 'timestamp' })
    createdAt: Date;

}