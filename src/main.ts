import { NestFactory } from '@nestjs/core';
import { join } from 'path';
import { Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';
import * as timeout from 'connect-timeout';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useStaticAssets(join(__dirname + './../public'));
  app.setBaseViewsDir(join(__dirname + './../views'));
  app.setViewEngine('hbs');

  app.use(timeout('500s'));

  app.connectMicroservice({
    transport: Transport.TCP,
    options: { retryAttempts: 5, retryDelay: 3000 },
  });

  await app.startAllMicroservicesAsync();

  await app.listen(3001);
}
bootstrap();
