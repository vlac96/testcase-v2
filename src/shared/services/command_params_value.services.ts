import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CommandParamsValue } from 'entities/command_params_value.entity';
import { CommandParamsInfoService } from './command_params_info.service';

@Injectable()
export class CommandParamsValueService {
    constructor(
        @InjectRepository(CommandParamsValue)
        private readonly repository: Repository<CommandParamsValue>,
        private readonly commandParamsInfoService: CommandParamsInfoService,
    ) { }

    async save(data, commandParamInfoId): Promise<any> {
        try {
            data.command_params_info = await this.commandParamsInfoService.findById(commandParamInfoId);
            return await this.repository.save(data);
        } catch (err) {
            throw new Error('Save CommandParamsValue Errors' + err + '  ->>' + data);
        }
    }

    async delete(id): Promise<any> {
        try {
            return await this.repository.delete(id);
        } catch (err) {
            throw new Error('Delete CommandParamsValue Errors' + err);
        }
    }

    async findOne(outputId): Promise<CommandParamsValue> {
        return await this.repository.findOne({ id: outputId }, { relations: ['testcase_command'] });
    }

    async removeByEntitis(entities): Promise<any> {
        return await this.repository.remove(entities);
    }

    async removeByTestcase_CommandStepsParams(testcaseCommand_id, commandStepParamsId): Promise<any> {
        return await this.repository.createQueryBuilder('command_params_value')
            .delete()
            .where(`command_params_value.testcaseCommandId = ${testcaseCommand_id}`)
            .andWhere(`command_params_value.stepsparamsId = ${commandStepParamsId}`)
            .execute();
    }

    replaceParamByValue(commandParamInfo, defaultActionValue) {
        let value;

        switch (commandParamInfo.param_replace_type) {
            case null:
            case '':
            case 'replace-all':
                value = commandParamInfo.command_params_value.length > 0 ? commandParamInfo.command_params_value[0].value : defaultActionValue;
                return value;
                break;

            case 'replace-partial':
                // tslint:disable-next-line:max-line-length
                value = commandParamInfo.command_params_value.length > 0 ? commandParamInfo.command_params_value[0].value : commandParamInfo.param_default_value;
                return defaultActionValue.replace(new RegExp('#param#', 'gi'), value);
                break;

            case 'insert-at-last':
                // tslint:disable-next-line:max-line-length
                value = commandParamInfo.command_params_value.length > 0 ? commandParamInfo.command_params_value[0].value : commandParamInfo.param_default_value;
                return defaultActionValue += value;
                break;

            default:
                return defaultActionValue;
                break;
        }
    }
}