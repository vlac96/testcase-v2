import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CommandActions } from '../../entities/command_actions.entity';
import { CommandParamsInfoService } from './command_params_info.service';
import { CommandParamsValueService } from './command_params_value.services';

@Injectable()
export class CommandActionsService {
    constructor(
        @InjectRepository(CommandActions)
        private readonly repository: Repository<CommandActions>,
        private readonly commandParamsInfoService: CommandParamsInfoService,
        private readonly commandParamsValueService: CommandParamsValueService,
    ) { }

    async save(data, command): Promise<any> {
        try {
            data.command = command;
            const commandAction = await this.repository.save(data);

            for (const commandParamInfo of data.command_params_info) {
                await this.commandParamsInfoService.save(commandParamInfo, commandAction);
            }

            return commandAction;
        } catch (err) {
            throw new Error('Save CommandActions Errors' + err + '  ->>' + data);
        }
    }

    async delete(id): Promise<any> {
        try {
            return await this.repository.delete(id);
        } catch (err) {
            throw new Error('Delete CommandActions Errors' + err);
        }
    }

    async findOne(outputId): Promise<CommandActions> {
        return await this.repository.findOne({ id: outputId }, {relations: ['step_params']});
    }

    replaceParamByValue(commandAction) {
        if (commandAction.command_params_info.length === 0) {
            return commandAction;
        }

        for (const commandParamInfo of commandAction.command_params_info) {
            // tslint:disable-next-line:max-line-length
            commandAction[commandParamInfo.attributeTo] = this.commandParamsValueService.replaceParamByValue(commandParamInfo, commandAction[commandParamInfo.attributeTo]);
        }

        return commandAction;
    }
}