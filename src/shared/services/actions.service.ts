import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TestcaseActions } from '../../entities/testcase_actions.entity';

import { ScriptParamsValueService } from './script_params_value.service';
import { CommandParamsValueService } from './command_params_value.services';

@Injectable()
export class ActionsService {
    constructor(
        @InjectRepository(TestcaseActions)
        private readonly repository: Repository<TestcaseActions>,
        private readonly commandParamsValueService: CommandParamsValueService,
        private readonly scriptParamsValueService: ScriptParamsValueService,
    ) { }

    async save(data, testcase): Promise<any> {
        try {
            data.testcase = testcase;
            if (data.script !== null) {
                // Save script params value
                const scriptParamsValue = [];
                for (const script_param_info of data.script.script_params_info) {
                    for (const script_param_value of script_param_info.script_params_value) {
                        const scriptParamValue = await this.scriptParamsValueService.save(script_param_value, script_param_info.id);
                        scriptParamsValue.push(scriptParamValue);
                    }
                }
                data.script_params_value = scriptParamsValue;
            } else if (data.command !== null) {
                // Save command_params_value
                const commandParamsValue = [];
                for (const command_action of data.command.command_actions) {
                    for (const command_param_info of command_action.command_params_info) {
                        if (command_param_info.command_params_value.length > 0) {
                            const commandParamValue = await this.commandParamsValueService.save(
                                command_param_info.command_params_value[0], command_param_info.id);

                            commandParamsValue.push(commandParamValue);
                        }
                    }
                }
                data.command_params_value = commandParamsValue;
            }
            await this.repository.save(data);
        } catch (err) {
            throw new Error('Save TestcaseActions Errors' + err);
        }
    }

    async delete(id): Promise<any> {
        try {
            await this.repository.delete(id);
        } catch (err) {
            throw new Error('Delete TestcaseActions Errors' + err);
        }
    }

    async findOne(actionId): Promise<TestcaseActions> {
        const step = await this.repository.findOne({ id: actionId });

        if (typeof step === 'undefined') {
            return new TestcaseActions();
        }
        return step;
    }
}