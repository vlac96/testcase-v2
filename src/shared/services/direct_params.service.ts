import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DirectParams } from '../../entities/direct_params.entity';

@Injectable()
export class DirectParamsService {
    constructor(
        @InjectRepository(DirectParams)
        private readonly repository: Repository<DirectParams>,
    ) { }

    async getQuery(query): Promise<[DirectParams[], number]> {
        let result = await this.repository.createQueryBuilder('direct_params');

        if (query.search_term) {
            const _search_term = '%' + query.search_term + '%';
            result = result.where(`direct_params.name LIKE '${_search_term}' OR direct_params.id = '${query.search_term}'`);
        }

        return result.orderBy('direct_params.name', 'ASC')
            .limit(Number(query.perPage) || 10)
            .offset(Number(query.perPage * query.pageNum) || 0)
            .getManyAndCount();
    }

    async save(data): Promise<any> {
        try {
            await this.repository.save(data);
        } catch (err) {
            throw new Error('Save DirectParams Errors' + err);
        }
    }

    async delete(ids): Promise<any> {
        try {
            for (const id of ids) {
                await this.repository.delete(id);
            }
        } catch (err) {
            throw new Error('Delete DirectParams Errors' + err);
        }
    }

    async findById(directParamId): Promise<DirectParams> {
        const directParam = await this.repository.findOne({ id: directParamId });

        return directParam;
    }

    async findByName(directParamName): Promise<DirectParams> {
        const directParam = await this.repository.findOne({ name: directParamName });

        return directParam;
    }

    async replaceParamAlias(data) {
        const cloneData = { ...data };
        for (const key in cloneData) {
            if (cloneData.hasOwnProperty(key) && (typeof cloneData[key] === 'string')) {
                const rexg = /{.+?}/gim;
                const found = cloneData[key].match(rexg);

                if (found === null) {
                    continue;
                }

                for (const subFound of found) {
                    let defaultParamName = subFound.match(/(\w)/g);
                    defaultParamName = defaultParamName.join('');

                    const defaultParamFounded = await this.findByName(defaultParamName);

                    if (typeof defaultParamFounded !== 'undefined') {
                        cloneData[key] = cloneData[key].replace(subFound, defaultParamFounded.value);
                    }
                }
            }
        }

        return cloneData;
    }

    async prepareExportDirectParams() {
        const fs = require('fs-extra');
        const filePath = __dirname + './../../../public/directParams.json';
        const data = [];
        const DirectParams = await this.repository.find();

        data.push(...DirectParams);

        fs.outputFile(filePath, JSON.stringify(data), (err) => {
            if (err !== null) {
                // console.log(err);
            }
        });
    }

    async importJson(file) {
        const data = file.buffer.toString('utf8');
        for (const directParam of JSON.parse(data)) {
            await this.save(directParam);
        }
    }
}