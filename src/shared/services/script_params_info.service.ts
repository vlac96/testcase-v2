import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ScriptParamsInfo } from 'entities/script_params_info.entity';

@Injectable()
export class ScriptParamsInfoService {
    constructor(
        @InjectRepository(ScriptParamsInfo)
        private readonly repository: Repository<ScriptParamsInfo>,
    ) { }

    async save(data, script): Promise<any> {
        try {
            data.script = script;
            return await this.repository.save(data);
        } catch (err) {
            throw new Error('Save ScriptParamsInfo Errors' + err + '  ->>' + data);
        }
    }

    async delete(id): Promise<any> {
        try {
            await this.repository.delete(id);
        } catch (err) {
            throw new Error('Delete ScriptParamsInfo Errors' + err);
        }
    }

    async findById(scriptParamsInfoId): Promise<ScriptParamsInfo> {
        return await this.repository.findOne({ id: scriptParamsInfoId });
    }
}