import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Scripts } from 'entities/scripts.entity';
import { ScriptParamsInfoService } from './script_params_info.service';

@Injectable()
export class ScriptsService {
    constructor(
        @InjectRepository(Scripts)
        private readonly repository: Repository<Scripts>,
        private readonly scriptParamsInfoService: ScriptParamsInfoService,
    ) { }

    async getById(scriptId): Promise<Scripts> {
        return await this.repository.createQueryBuilder('scripts')
            .leftJoinAndSelect('scripts.script_params_info', 'script_params_info')
            .leftJoinAndSelect('scripts.project', 'project')
            .where('scripts.id = :id', {id: scriptId})
            .getOne();
    }

    async getQuery(query): Promise<any> {
        let res = await this.repository.createQueryBuilder('scripts')
        .leftJoinAndSelect('scripts.project', 'projects');

        if (query.search_term !== null && query.search_term !== '') {
            const _search_term = '%' + query.search_term + '%';
            res = res.andWhere(`scripts.name LIKE '${_search_term}'`);
        }

        // if (query.project && query.project !== null) {
        //     res = res.andWhere(`script.projectId = ${query.project}`);
        // }
        if (query.project !== null && query.project !== '') {
            const _project_id = query.project;
            if (query.project === 'none') {
                res = res.andWhere('projectId IS NULL');
            } else {
                res = res.andWhere('projects.id = :_project_id', { _project_id });
            }
        }

        if (query.search_term === null || query.project === null) {
            res = res.skip(query.pageNum * query.perPage)
                .take(query.perPage);
        }

        return res.orderBy('scripts.name', 'ASC').getManyAndCount();
    }

    async save(data): Promise<any> {
        try {
            const script = await this.repository.save(data);

            for (const scriptParamInfo of data.script_params_info) {
                await this.scriptParamsInfoService.save(scriptParamInfo, {...script});
            }

            return await this.getById(script.id);
        } catch (err) {
            throw new Error('Save Scripts Errors ' + err + '  ->>' + data);
        }
    }

    async deleteMulti(ids): Promise<any> {
        try {
            for (const id of ids) {
                await this.repository.delete(id);
            }
        } catch (err) {
            throw new Error('Delete Scripts Errors' + err);
        }
    }

    async getAll(): Promise<Scripts[]> {
        return await this.repository.find({relations: ['project']});
    }

    async prepareExportScripts(ids) {
        const fs = require('fs-extra');
        const filePath = __dirname + './../../../public/scripts.json';
        const data = [];
        if (ids.data === 'all') {
            const scripts = await this.repository.createQueryBuilder('scripts')
            .leftJoinAndSelect('scripts.script_params_info', 'script_params_info')
            .leftJoinAndSelect('scripts.project', 'project')
            .getMany();

            data.push(...scripts);
        } else {
            for (const scId of ids) {
                const scripts = await this.getById(scId);

                data.push(scripts);
            }
        }

        fs.outputFile(filePath, JSON.stringify(data), (err) => {});
    }

    async importJson(file) {
        const data = file.buffer.toString('utf8');
        for (const script of JSON.parse(data)) {
            await this.save(script);
        }
    }
}