import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Projects } from '../../entities/project.entity';

@Injectable()
export class ProjectService {
    constructor(
        @InjectRepository(Projects)
        private readonly projectRepository: Repository<Projects>,
    ) { }

    async getProjectsOptions(): Promise<Projects[]> {
        return this.projectRepository.find({order: {createdAt: 'DESC'}});
    }

    async getProjects(query): Promise<[Projects[], number]> {
        try {
            let res = await this.projectRepository.createQueryBuilder('projects');

            if (query.search_term !== null && query.search_term !== '') {
                const _search_term = '%' + query.search_term + '%';
                res = res.andWhere('projects.name LIKE :_search_term', { _search_term });
            }

            if (query.search_term === null) {
                res = res.skip(query.pageNum * query.perPage)
                .take(query.perPage);
            }

            return res.orderBy('projects.createdAt', 'DESC').getManyAndCount();
        } catch (err) {
            throw new Error('Get Projects Error ' + err);
        }
    }

    async save(data): Promise<any> {
        try {
            const res = await this.projectRepository.save(data);
            return res;
        } catch (err) {
            throw new Error('Save Project Errors' + err);
        }
    }

    async delete(id): Promise<any> {
        try {
            return await this.projectRepository.delete(id);
        } catch (err) {
            throw new Error('Delete Project Errors' + err);
        }
    }

    async findOne(projectId): Promise<any> {
        const project = await this.projectRepository.findOne({ id: projectId });

        return project;
    }

    async findName(projectName): Promise<any> {
        return await this.projectRepository.findOne({name: projectName});
    }
}