import { Injectable } from '@nestjs/common';
import * as webdriverio from 'webdriverio';
import * as _ from 'lodash';

@Injectable()
export class ClientsService {
    screenConfig = require('../../../screenconfig.json');
    isMobileMode = false;
    client;
    compareVariable = {};
    constructor(
    ) {
        // const options = {
        //     desiredCapabilities: {
        //         browserName: 'chrome',
        //         chromeOptions: {
        //             args: [
        //                 // 'headless',
        //                 // 'disable-gpu',
        //             ],

        //             // options for mobile
        //             // args: [
        //             //     'use-mobile-user-agent',
        // tslint:disable-next-line:max-line-length
        //             //     'user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3',
        //             // ],
        //         },

        //         // for firefox
        //         // browserName: 'firefox',
        //         // firefoxOptions: {
        //         //     // binary: 'C:/Program Files/Firefox Developer Edition/firefox.exe',
        //         //     loggingPrefs: './public/log.txt',
        //         // },
        //     },
        //     deprecationWarnings: false,
        //     // logLevel: 'verbose',
        // };
        // this.client = webdriverio.remote(options);
    }
    getChromeOptions(isMobileMode) {
        let chrome_options;
        if (isMobileMode) {
            chrome_options = {
                args: [
                    'use-mobile-user-agent',
                    // tslint:disable-next-line:max-line-length
                    'user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3',
                ],
            };
        } else {
            chrome_options = {
                args: [
                    // 'headless',
                    // 'disable-gpu',
                ],
            };
        }

        return chrome_options;
    }
    initClient(isMobileMode) {
        const options = {
            desiredCapabilities: {
                browserName: 'chrome',
                chromeOptions: this.getChromeOptions(isMobileMode),
            },
            deprecationWarnings: false,
        };
        this.client = webdriverio.remote(options);
    }
    async checkCookieClient(isMobileMode) {
        try {
            await this.client.getCookie();
            if (isMobileMode !== this.isMobileMode) {
                this.isMobileMode = isMobileMode;
                this.initClient(isMobileMode);

                await this.clientKiller();
                await this.setViewPort(this.screenConfig.width, this.screenConfig.height, isMobileMode);
            }
        } catch (e) {
            this.initClient(isMobileMode);

            await this.clientKiller();
            await this.setViewPort(this.screenConfig.width, this.screenConfig.height, isMobileMode);
        }
    }
    async run(testcase, keepSession = false) {
        // console.log(testcase, 'testcase');
        const result = { id: testcase.id, name: testcase.name, actions: [], outputs: [] };
        if (keepSession === false || this.client.requestHandler.sessionID === null) {
            this.initClient(testcase.is_mobile_mode);
            await this.clientKiller();
            await this.setViewPort(this.screenConfig.width, this.screenConfig.height, testcase.is_mobile_mode);
        }

        for (const action of testcase.actions) {
            try {
                const stepsResult = await this.runSteps(action);
                result.actions.push(...stepsResult);
            } catch (err) {
                try {
                    await this.client.getCookie();
                } catch (e) {
                    result.actions.push({ input: action.order, message: 'Chrome has been close!', status: 0 });
                    await this.clientKiller();
                    return { ...result, continue: false };
                }
                result.actions.push({ input: action.order, message: err.message, status: 0 });

                return { ...result, continue: true };
            }
        }

        for (const output of testcase.outputs) {
            if (output.is_compare_another_output && output.actions_compared_order) {
                output.has_value = this.compareVariable[output.actions_compared_order];
            }

            try {
                const outputResult = await this.runOutputs(output);

                result.outputs.push(...outputResult);
            } catch (err) {
                result.outputs.push({ output: Number(output.order) + 1, message: err.message, status: 0 });

                continue;
            }
        }
        // reset compareVariable
        this.compareVariable = {};
        return { ...result, continue: true };
    }

    async setViewPort(viewWidth, viewHeight, isMobileMode) {
        if (isMobileMode) {
            viewWidth = 536;
            viewHeight = 674;
        }
        await this.client.init().setViewportSize({
            width: viewWidth,
            height: viewHeight,
        });
    }

    calculate(result, out) {
        if (_.includes(['>', '<', '>=', '<='], out.operator)) {
            result = Number.isNaN(result) ? 0 : Number(result);
            out.has_value = Number.isNaN(out.has_value) ? 0 : Number(out.has_value);
        }
        switch (out.operator) {
            case '=':
                if ((typeof result !== 'undefined' && typeof out.has_value !== 'undefined') &&
                        result.toString().trim() === out.has_value.toString().trim()) 
                    return 1;
                return 0;
            case '!=':
                if (result !== out.has_value)
                    return 1;
                return 0;
            case 'contain':
                if (result.includes(out.has_value))
                    return 1;
                return 0;
            case 'not_contain':
                if (!result.includes(out.has_value))
                    return 1;
                return 0;
            case '>':
                if (result > out.has_value)
                    return 1;
                return 0;
            case '>=':
                if (result >= out.has_value)
                    return 1;
                return 0;
            case '<':
                if (result < out.has_value)
                    return 1;
                return 0;
            case '<=':
                if (result <= out.has_value)
                    return 1;
                return 0;
            default:
                break;
        }
    }

    async clientKiller() {
        try {
            await this.client.endAll();
        } catch (e) {
            // tslint:disable-next-line:no-console
            console.log(e, 'Kill session Error at client.service.ts 163: clientKiller function');
        }
    }

    async runMulti(data) {
        const result = [];

        const temp_result = await this.run(data.testcases, data.keepSession);
        result.push({ ...temp_result });

        return result;
    }

    async runSteps(step) {
        const result = [];
        switch (step.action) {
            case 'url':
                await this.client.url(step.value);
                result.push({ input: step.order, message: 'URL success', status: 1 });
                break;
            case 'refresh-page':
                await this.client.refresh().pause(step.waiting_time);
                result.push({ input: step.order, message: 'Refresh Page success', status: 1 });
                break;
            case 'clear':
                await this.client.pause(500).clearElement(step.element)
                    .pause(step.waiting_time);
                result.push({ input: step.order, message: 'Clear value success', status: 1 });
                break;
            case 'input':
                // await this.client.elements(step.element).then((res) => {
                //     const elementIdValueCommands = [];
                //     for (const elem of res.value) {
                //         elementIdValueCommands.push(
                //             this.client.elementIdClear(elem.ELEMENT)
                //                 .pause(300)
                //                 .elementIdValue(elem.ELEMENT, step.value));
                //     }
                //     return this.client.unify(elementIdValueCommands);
                // });
                // result.push({ input: step.order, message: 'Set input value success', status: 1 });
                // break;
                await this.client.waitForEnabled(step.element, step.waiting_time)
                    .clearElement(step.element)
                    .pause(300)
                    .setValue(step.element, step.value).pause(300);
                result.push({ input: step.order, message: 'Set input value success', status: 1 });
                break;

            case 'pause':
                await this.client.pause(Number(step.value) || 0);
                result.push({ input: step.order, message: 'Pause success', status: 1 });
                break;

            case 'keys':
                await this.client.pause(500).keys(step.value);
                result.push({ input: step.order, message: 'Type keyboard success', status: 1 });
                break;

            case 'upload':
                step.value = step.value.replace(/\\/gi, '/');
                await this.client.waitForExist(step.element)
                    .chooseFile(step.element, step.value)
                    .scroll(step.element);
                result.push({ input: step.order, message: 'Upload file success', status: 1 });
                break;

            case 'click':
                await this.client.waitForEnabled(step.element, step.waiting_time)
                    .click(step.element).pause(1000);
                result.push({ input: step.order, message: 'Click success', status: 1 });
                break;

            case 'middle-click':
                await this.client.waitForEnabled(step.element, step.waiting_time)
                    .middleClick(step.element).pause(1000);
                result.push({ input: step.order, message: 'Click center success', status: 1 });
                break;

            case 'move-to-object':
                await this.client.moveToObject(step.element, step.xFrom, step.yFrom)
                    .scroll();
                result.push({ input: step.order, message: 'Move to Element Success', status: 1 });
                break;

            case 'drag-drop':
                if (!step.xFrom) step.xFrom = 0;
                if (!step.yFrom) step.yFrom = 0;
                // console.log({ x: Number(step.xFrom), y: Number(step.yFrom) }, 'a');
                await this.client.pause(step.waiting_time)
                    .execute((elemntFrom, ElementTo, offset) => {
                        function triggerEvent(elem, eventName, eventType, x, y) {
                            var event = document.createEvent(eventType);
                            if (x) {
                                event.initMouseEvent(eventName, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
                            } else {
                                event.initEvent(eventName, true, true);
                            }

                            elem.dispatchEvent(event);
                        }

                        let dragElem = $(elemntFrom)[0];
                        let dropElem = $(ElementTo)[0];
                        triggerEvent(dragElem, 'mousedown', 'MouseEvent');
                        triggerEvent(dragElem, 'dragstart', 'MouseEvent');
                        triggerEvent(
                            dropElem, 'drop', 'MouseEvent',
                            $(ElementTo).offset().left - $(elemntFrom).offset().left + offset.x,
                            $(ElementTo).offset().top - $(elemntFrom).offset().top + offset.y,
                        );
                        triggerEvent(dragElem, 'dragend', 'MouseEvent');
                    }, step.element, step.elementTo, { x: Number(step.xFrom), y: Number(step.yFrom) });
                result.push({ input: step.order, message: 'Drag drop success', status: 1 });
                break;

            case 'move-element':
                await this.client.waitForVisible(step.element, step.waiting_time)
                    .click(step.element, 5, 5)
                    .pause(500)
                    .execute((element, offset) => {
                        function triggerEvent(elem, eventName, eventType, x, y) {
                            var event = document.createEvent(eventType);
                            if (x) {
                                event.initMouseEvent(eventName, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
                            } else {
                                event.initEvent(eventName, true, true);
                            }

                            return event;
                        }

                        var moveElem = $(element)[0];

                        var eventOnDragstart = new CustomEvent('dragstart', {
                            detail: {
                                p: {
                                    x: $(element).offset().left,
                                    y: $(element).offset().top
                                },
                                event: triggerEvent(moveElem, 'dragstart', 'MouseEvent')
                            }
                        });
                        var eventOnDragmove = new CustomEvent('dragmove', {
                            detail: {
                                p: {
                                    x: $(element).offset().left + offset.x,
                                    y: $(element).offset().top + offset.y
                                },
                                event: triggerEvent(moveElem, 'dragmove', 'MouseEvent', offset.x, offset.y)
                            }
                        });

                        var eventOnDragend = new CustomEvent('dragend');

                        moveElem.dispatchEvent(eventOnDragstart);
                        moveElem.dispatchEvent(eventOnDragmove);
                        moveElem.dispatchEvent(eventOnDragend);
                    }, step.element, { x: Number(step.xFrom), y: Number(step.yFrom) });
                result.push({ input: step.order, message: 'Move element success', status: 1 });
                break;

            case 'resize':
                await this.client.waitForVisible(step.element, step.waiting_time)
                    .pause(500)
                    .execute((element, offset) => {
                        function triggerEvent(elem, eventName, eventType, x, y) {
                            var event = document.createEvent(eventType);
                            if (x) {
                                event.initMouseEvent(eventName, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
                            } else {
                                event.initEvent(eventName, true, true);
                            }

                            return event;
                        }

                        function getEvent(elem, eventName, eventType, x, y) {
                            var event = new CustomEvent(eventName, {
                                detail: {
                                    x: x,
                                    y: y
                                },
                            });
                            event.event = triggerEvent(elem, eventName, 'MouseEvent', x, y);
                            return event;
                        }
                        var rect = SVG.select(element);
                        var elmentTest = document.querySelector(element);

                        rect.selectize({
                            classRect: 'svg_select_boundingRect',
                            deepSelect: true,
                            points: true,
                            rotationPoint: true,
                            radius: 30
                        }).resize()

                        event = getEvent(
                            elmentTest, 'mousemove.resize', 'MouseEvent',
                            $(elmentTest).offset().left,
                            $(elmentTest).offset().top,
                        );

                        rect.fire('resizestart', {
                            dx: event.detail.x,
                            dy: event.detail.y,
                            event: event,
                        });
                        rect.size($(element).width() + offset.x, $(element).height() + offset.y);
                        rect.fire('resizing', {
                            dx: $(elmentTest).offset().left,
                            dy: $(elmentTest).offset().top,
                            event: event,
                        });
                        rect.fire('resizedone');
                    }, step.element, { x: Number(step.xFrom), y: Number(step.yFrom) });
                result.push({ input: step.order, message: 'Resize element success', status: 1 });
                break;

            case 'rotate':
                await this.client.waitForVisible(step.element, step.waiting_time)
                    .pause(500)
                    .execute((element, offset) => {
                        function triggerEvent(elem, eventName, eventType, x, y) {
                            var event = document.createEvent(eventType);
                            if (x) {
                                event.initMouseEvent(eventName, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
                            } else {
                                event.initEvent(eventName, true, true);
                            }

                            return event;
                        }

                        function getEvent(elem, eventName, eventType, x, y) {
                            var event = new CustomEvent(eventName, {
                                detail: {
                                    x: x,
                                    y: y
                                },
                            });
                            event.event = triggerEvent(elem, eventName, 'MouseEvent', x, y);
                            return event;
                        }
                        var rect = SVG.select(element);
                        var elmentTest = document.querySelector(element);

                        rect.selectize({
                            classRect: 'svg_select_boundingRect',
                            deepSelect: true,
                            points: true,
                            rotationPoint: true,
                            radius: 30
                        }).resize()

                        event = getEvent(
                            elmentTest, 'mousemove.resize', 'MouseEvent',
                            $(elmentTest).offset().left,
                            $(elmentTest).offset().top,
                        );

                        rect.fire('resizestart', {
                            dx: event.detail.x,
                            dy: event.detail.y,
                            event: event,
                        });
                        rect.transform({ rotation: offset.x }, true);
                        rect.fire('resizing', {
                            dx: $(elmentTest).offset().left,
                            dy: $(elmentTest).offset().top,
                            event: event,
                        });
                        rect.fire('resizedone');
                    }, step.element, { x: Number(step.xFrom), y: Number(step.yFrom) });
                result.push({ input: step.order, message: 'Rotate success', status: 1 });
                break;

            case 'click-dropdown':
                await this.client
                    .scroll(step.element)
                    .pause(1000)
                    .execute((element) => {
                        function triggerEvent(elem, eventName, eventType, x, y) {
                            var event = document.createEvent(eventType);
                            if (x) {
                                event.initMouseEvent(eventName, true, true, window, 0, 0, 0, x, y, false, false, false, false, 0, null);
                            } else {
                                event.initEvent(eventName, true, true);
                            }

                            elem.dispatchEvent(event);
                        }

                        var elem = $(element)[0];
                        triggerEvent(elem, 'click', 'MouseEvent', 0, 0);

                    }, step.element);
                result.push({ input: step.order, message: 'Click drop-down success', status: 1 });
                break;

            case 'r-click':
                await this.client.waitForExist(step.element, step.waiting_time)
                    .rightClick(step.element);
                result.push({ input: step.order, message: 'Right click success', status: 1 });
                break;

            case 'select-by-visible-text':
                await this.client.waitForExist(step.element, step.waiting_time)
                    .selectByVisibleText(step.element, step.value);
                result.push({ input: step.order, message: 'Select dropdown value by text success', status: 1 });
                break;

            case 'd-click':
                await this.client.waitForExist(step.element, step.waiting_time)
                    .doubleClick(step.element);
                result.push({ input: step.order, message: 'Double click success', status: 1 });
                break;

            case 'checked':
                await this.client.waitForEnabled(step.element, step.waiting_time);

                const checkedScriptResult = await this.client.execute((cloneStep) => {
                    return $(cloneStep.element)[0].checked;
                }, step);

                if (!checkedScriptResult.value) {
                    try {
                        await this.client.pause(1000).click(step.element);
                    } catch (err) {
                        const checkAgain = await this.client.execute((cloneStep) => {
                            return $(cloneStep.element)[0].checked;
                        }, step);

                        if (!checkAgain.value) {
                            const n = step.element.lastIndexOf('>');
                            const res = step.element.substring(0, n - 1);
                            await this.client.pause(1000).click(res);
                        }
                    }
                }

                result.push({ input: step.order, message: 'Tick checkbox success', status: 1 });
                break;

            case 'unchecked':
                await this.client.waitForEnabled(step.element, step.waiting_time);

                const uncheckedScriptResult = await this.client.execute((cloneStep) => {
                    return $(cloneStep.element)[0].checked;
                }, step);

                if (uncheckedScriptResult.value) {
                    try {
                        await this.client.pause(1000).click(step.element);
                    } catch (err) {
                        const checkAgain = await this.client.execute((cloneStep) => {
                            return $(cloneStep.element)[0].checked;
                        }, step);

                        if (checkAgain.value) {
                            const n = step.element.lastIndexOf('>');
                            const res = step.element.substring(0, n - 1);
                            await this.client.pause(1000).click(res);
                        }
                    }
                }
                result.push({ input: step.order, message: 'Untick checkbox success', status: 1 });
                break;

            case 'script':
                await this.client.pause(step.waiting_time);
                const scriptResult = await this.client.execute((output) => {
                    // tslint:disable-next-line:no-eval
                    return eval(output.element);
                }, step);

                this.compareVariable[step.order] = scriptResult.value;
                result.push({
                    input: step.order,
                    message: 'Run script and save data return at ' + step.order + ' success, value: ' + scriptResult.value,
                    status: 1,
                });
                break;
        }

        return result;
    }

    async runOutputs(out) {
        const result = [];
        switch (out.type) {
            case 'url':
                const result_url = await this.client.getUrl();

                if (result_url === out.has_value) {
                    result.push({ output: Number(out.order) + 1, message: result_url + '| respect: ' + out.has_value, status: 1 });
                }
                else {
                    result.push({ output: Number(out.order) + 1, message: result_url + '| respect: ' + out.has_value, status: 0 });
                }

                break;

            case 'title':

                const title = await this.client.getTitle();

                result.push({ output: Number(out.order) + 1, message: title + '| respect: ' + out.has_value, status: this.calculate(title, out) });
                break;

            case 'text':

                const text = await this.client.waitForExist(out.element, out.waiting_time)
                    .getText(out.element);
                result.push({ output: Number(out.order) + 1, message: text, status: this.calculate(text, out) });
                break;

            case 'visible':

                const isVisible = await this.client.pause(out.waiting_time).isVisible(out.element);
                if (isVisible) {
                    result.push({ output: Number(out.order) + 1, message: 'Visible', status: 1 });
                }
                else {
                    result.push({ output: Number(out.order) + 1, message: 'Not Visible', status: 0 });
                }

                break;

            case 'enabled':
                const isEnabled = await this.client.pause(out.waiting_time).isEnabled(out.element);
                if (isEnabled) {
                    result.push({ output: Number(out.order) + 1, message: 'Enabled', status: 1 });
                }
                else {
                    result.push({ output: Number(out.order) + 1, message: 'Not Enabled', status: 0 });
                }

                break;

            case 'exist':

                const isExist = await this.client.pause(out.waiting_time).isExisting(out.element);
                if (isExist) {
                    result.push({ output: Number(out.order) + 1, message: 'Exist', status: 1 });
                }
                else {
                    result.push({ output: Number(out.order) + 1, message: 'Not Exist', status: 0 });
                }

                break;

            case 'not-visible':

                const is_visible = await this.client.pause(out.waiting_time).isVisible(out.element);
                if (!is_visible) {
                    result.push({ output: Number(out.order) + 1, message: 'Not Visible', status: 1 });
                }
                else {
                    result.push({ output: Number(out.order) + 1, message: 'Visible', status: 0 });
                }

                break;

            case 'not-enabled':

                const is_enabled = await this.client.pause(out.waiting_time).isEnabled(out.element);
                if (!is_enabled) {
                    result.push({ output: Number(out.order) + 1, message: 'Not Enabled', status: 1 });
                }
                else {
                    result.push({ output: Number(out.order) + 1, message: 'Enabled', status: 0 });
                }

                break;

            case 'not-exist':

                const is_exist = await this.client.pause(out.waiting_time).isExisting(out.element);

                if (!is_exist) {
                    result.push({ output: Number(out.order) + 1, message: 'Not Exist', status: 1 });
                }
                else {
                    result.push({ output: Number(out.order) + 1, message: 'Exist', status: 0 });
                }

                break;

            case 'script':
                await this.client.pause(out.waiting_time);
                const scriptResult = await this.client.execute((output) => {
                    // tslint:disable-next-line:no-eval
                    return eval(output.element);
                }, out);

                if (typeof scriptResult.value !== 'undefined') {
                    result.push({
                        output: Number(out.order) + 1,
                        message: scriptResult.value + ' | has_value: ' + out.has_value,
                        status: this.calculate(scriptResult.value.toString(), out),
                    });
                } else {
                    result.push({
                        output: Number(out.order) + 1,
                        message: scriptResult.value + ' | has_value: ' + out.has_value,
                        status: 0,
                    });
                }

                break;
        }

        return result;
    }
}