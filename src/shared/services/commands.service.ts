import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as _ from 'lodash';

import { Commands } from 'entities/command.entity';

import { CommandActionsService } from './command_actions.service';

@Injectable()
export class CommandsService {
    constructor(
        @InjectRepository(Commands)
        private readonly repository: Repository<Commands>,
        private readonly commandActionsService: CommandActionsService,
    ) { }

    async getAll(): Promise<Commands[]> {
        return this.repository.find({
            order: {
                name: 'ASC',
            },
            relations: ['project'],
        });
    }

    async getQuery(query) {
        try {
            let res = await this.repository.createQueryBuilder('commands')
                .leftJoinAndSelect('commands.project', 'projects');

            if (query.search_term !== null && query.search_term !== '') {
                const _search_term = '%' + query.search_term + '%';
                // res = res.andWhere(`commands.name LIKE '${query.search_term}' OR commands.id = '${query.search_term}'`);
                res = res.andWhere(`commands.name LIKE '%${query.search_term}%'`);
            }

            if (query.project !== null && query.project !== '') {
                const _project_id = query.project;
                if (query.project === 'none') {
                    res = res.andWhere('projectId IS NULL');
                } else {
                    res = res.andWhere('projects.id = :_project_id', { _project_id });
                }
            }

            if (query.search_term === null && query.project === null) {
                res = res.skip(query.pageNum * query.perPage)
                    .take(query.perPage);
            }

            return res.orderBy('commands.name', 'ASC').getManyAndCount();
            // return res.orderBy('commands.name', 'ASC').getQuery();
        } catch (err) {
            throw new Error('Get Commands Error ' + err);
        }
    }

    async findById(commandId): Promise<Commands> {
        return await this.repository.createQueryBuilder('commands')
            .leftJoinAndSelect('commands.project', 'project')
            .leftJoinAndSelect('commands.command_actions', 'command_actions')
            .leftJoinAndSelect('command_actions.command_params_info', 'command_params_info')
            .orderBy('command_actions.order', 'ASC')
            .where('commands.id = :id', { id: commandId })
            .getOne();
    }

    async save(data): Promise<any> {
        try {
            const command = await this.repository.save(data);

            for (const commandAction of data.command_actions) {
                await this.commandActionsService.save(commandAction, command);
            }
            return this.findById(command.id);
        } catch (err) {
            // tslint:disable-next-line:no-console
            console.log(err, 'Save Command Error');
            return {};
        }
    }

    async delete(id): Promise<any> {
        try {
            return await this.repository.delete(id);
        } catch (err) {
            throw new Error('Delete Commands Errors' + err);
        }
    }

    async findByName(commandName): Promise<Commands> {
        const command = await this.repository.findOne({ name: commandName });

        return command;
    }

    async prepareExportCommands(ids) {
        const fs = require('fs-extra');
        const filePath = __dirname + './../../../public/commands.json';
        const data = [];
        if (ids.data === 'all') {
            const commands = await this.repository.createQueryBuilder('commands')
                .leftJoinAndSelect('commands.command_actions', 'command_actions')
                .leftJoinAndSelect('commands.testcases_commands', 'testcases_commands')
                .leftJoinAndSelect('commands.project', 'project')
                .leftJoinAndSelect('command_actions.command_params_info', 'command_params_info')
                .orderBy('command_actions.order', 'ASC')
                .getMany();

            data.push(...commands);
        } else {
            for (const cmid of ids) {
                const command = await this.findById(cmid);

                data.push(command);
            }
        }

        fs.outputFile(filePath, JSON.stringify(data), (err) => {});
    }

    async importJson(file) {
        const data = file.buffer.toString('utf8');
        for (const command of JSON.parse(data)) {
            await this.save(command);
        }
    }

    async deleteMulti(ids) {
        for (const id of ids) {
            await this.repository.delete(id);
        }
    }
}