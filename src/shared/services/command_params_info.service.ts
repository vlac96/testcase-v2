import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CommandParamsInfo } from '../../entities/command_params_info.entity';

@Injectable()
export class CommandParamsInfoService {
    constructor(
        @InjectRepository(CommandParamsInfo)
        private readonly repository: Repository<CommandParamsInfo>,
    ) { }

    async save(data, commandAction): Promise<any> {
        try {
            data.command_action = commandAction;
            return await this.repository.save(data);
        } catch (err) {
            throw new Error('Save StepsParams Errors' + err + '  ->>' + data);
        }
    }

    async delete(id): Promise<any> {
        try {
            await this.repository.delete(id);
        } catch (err) {
            throw new Error('Delete StepsParams Errors' + err);
        }
    }

    async findById(stepsParamsId): Promise<CommandParamsInfo> {
        return await this.repository.findOne({ id: stepsParamsId });
    }

    filterCommandParamsInfo(commandActions) {
        for (const commandAction of commandActions) {
            if (commandAction.command_params_info.length > 0) {
                commandAction.command_params_info = commandAction.command_params_info.filter(item => item.checked === true);
            }
        }

        return commandActions;
    }
}