import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TestcaseRespectOutputs } from '../../entities/testcase_respect_outputs.entity';

import { ScriptParamsValueService } from './script_params_value.service';

@Injectable()
export class TestcaseRespectOutputsService {
    constructor(
        @InjectRepository(TestcaseRespectOutputs)
        private readonly outputRepository: Repository<TestcaseRespectOutputs>,
        private readonly scriptParamsValueService: ScriptParamsValueService,
    ) { }

    async save(data, testcase): Promise<any> {
        try {
            data.testcase = testcase;
            if (data.script !== null) {
                // Save script params value
                const scriptParamsValue = [];
                for (const script_param_info of data.script.script_params_info) {
                    for (const script_param_value of script_param_info.script_params_value) {
                        const scriptParamValue = await this.scriptParamsValueService.save(script_param_value, script_param_info.id);
                        scriptParamsValue.push(scriptParamValue);
                    }
                }
                data.script_params_value = scriptParamsValue;
            }
            await this.outputRepository.save(data);
        } catch (err) {
            throw new Error('Save TestcaseRespectOutputs Errors' + err);
        }
    }

    async delete(id): Promise<any> {
        try {
            await this.outputRepository.delete(id);
        } catch (err) {
            throw new Error('Delete TestcaseRespectOutputs Errors' + err);
        }
    }

    async findById(outputId): Promise<TestcaseRespectOutputs> {
        const output = await this.outputRepository.findOne({ id: outputId });

        return output;
    }

    // what
    async findOutputCompared(testcaseId, outputOrder): Promise<TestcaseRespectOutputs> {
        const output = await this.outputRepository.createQueryBuilder('testcase_respect_outputs')
            .where(`testcase_respect_outputs.testcaseId = ${testcaseId} AND testcase_respect_outputs.order = ${outputOrder}`)
            .getOne();

        return output;
    }
}