import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ScriptParamsValue } from 'entities/script_params_value.entity';
import { ScriptParamsInfoService } from './script_params_info.service';

@Injectable()
export class ScriptParamsValueService {
    constructor(
        @InjectRepository(ScriptParamsValue)
        private readonly repository: Repository<ScriptParamsValue>,
        private readonly scriptParamsInfoSerive: ScriptParamsInfoService,
    ) { }

    async save(data, scriptParamInfoId): Promise<any> {
        try {
            data.script_params_info = await this.scriptParamsInfoSerive.findById(scriptParamInfoId);
            return await this.repository.save(data);
        } catch (err) {
            throw new Error('Save ScriptParamsValue Errors' + err + '  ->>' + data);
        }
    }

    async delete(id): Promise<any> {
        try {
            await this.repository.delete(id);
        } catch (err) {
            throw new Error('Delete ScriptParamsValue Errors' + err);
        }
    }

    async findOne(outputId): Promise<ScriptParamsValue> {
        return await this.repository.findOne({ id: outputId });
    }
}