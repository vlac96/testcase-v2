import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Tags } from '../../entities/tags.entity';
import _ from 'lodash';
import 'reflect-metadata';

@Injectable()
export class TagsService {
    constructor(
        @InjectRepository(Tags)
        private readonly repository: Repository<Tags>,
    ) { }

    async getAll(): Promise<Tags[]> {
        try {
            return await this.repository.find({
                order: {
                    name: 'ASC',
                },
            });
        } catch (err) {
            throw new Error('Get Tags Error ' + err);
        }
    }

    async getQuery(query): Promise<[Tags[], number]> {
        try {
            let res = await this.repository.createQueryBuilder('tags');

            if (query.search_term !== null && query.search_term !== '') {
                const _search_term = '%' + query.search_term + '%';
                res = res.andWhere('tags.name LIKE :_search_term', { _search_term });
            }

            if (query.search_term === null) {
                res = res.skip(query.pageNum * query.perPage)
                    .take(query.perPage);
            }

            return res.getManyAndCount();
        } catch (err) {
            throw new Error('Get Tags Error ' + err);
        }
    }

    async save(data): Promise<any> {
        try {
            return await this.repository.save(data);
        } catch (err) {
            throw new Error('Save Tag Errors' + err);
        }
    }

    async delete(id): Promise<any> {
        try {
            return await this.repository.delete(id);
        } catch (err) {
            throw new Error('Delete Tag Errors' + err);
        }
    }

    async findById(tagId): Promise<any> {
        const tag = await this.repository.findOne({ id: tagId });

        return tag;
    }

    async findByIds(tagIds): Promise<Tags[]> {
        return await this.repository.findByIds(tagIds);
    }

    async findByName(tagName): Promise<any> {
        return await this.repository.findOne({ name: tagName });
    }
}