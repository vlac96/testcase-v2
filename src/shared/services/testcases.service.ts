import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as _ from 'lodash';

import { ActionsService } from './actions.service';
import { TestcaseRespectOutputsService } from './testcase_respect_outputs.service';
import { ProjectService } from './project.service';
import { TagsService } from './tags.service';
import { CommandActionsService } from './command_actions.service';
import { DirectParamsService } from './direct_params.service';

import { TestCases } from 'entities/testcase.entity';

@Injectable()
export class TestCasesService {
    constructor(
        @InjectRepository(TestCases)
        private readonly repository: Repository<TestCases>,
        private readonly actionsService: ActionsService,
        private readonly outputsService: TestcaseRespectOutputsService,
        private readonly projectService: ProjectService,
        private readonly tagsService: TagsService,
        private readonly commandActionsService: CommandActionsService,
        private readonly directParamsService: DirectParamsService,
    ) { }

    async getQuery(query): Promise<[TestCases[], number]> {
        let res = await this.repository.createQueryBuilder('testcases')
            .leftJoinAndSelect('testcases.project', 'projects')
            .leftJoinAndSelect('testcases.tags', 'tags');

        if (query.search_term !== null && query.search_term !== '') {
            const _search_term = '%' + query.search_term + '%';
            res = res.andWhere(`testcases.name LIKE '${_search_term}'`);
        }

        if (query.projectID !== null && query.projectID !== '') {
            const _project_id = query.projectID;
            if (query.projectID === 'none') {
                res = res.andWhere('projectId IS NULL');
            } else {
                res = res.andWhere('projects.id = :_project_id', { _project_id });
            }
        }

        if (query.tags.length > 0) {
            const tagsIds = query.tags;
            res = res.andWhere(qb => {
                const subQuery = qb.subQuery()
                    .select('pivot.testcasesId')
                    .from('testcases_tags_tags', 'pivot')
                    .where('pivot.tagsId IN (:...registered)', { registered: tagsIds })
                    .getQuery();
                return 'testcases.id IN ' + subQuery;
            });
        }
        if ((query.search_term === null || query.search_term === '') && query.projectID === null && query.tags.length === 0) {
            res = res.skip(query.pageNum * query.perPage)
                .take(query.perPage);
        }

        return res.orderBy('testcases.order', 'ASC').addOrderBy('testcases.id', 'ASC').getManyAndCount();
    }

    async findById(tcId): Promise<TestCases> {
        return await this.repository.createQueryBuilder('testcases')
            .leftJoinAndSelect('testcases.actions', 'actions')
            .leftJoinAndSelect('actions.script', 'action_scripts')
            .leftJoinAndSelect('action_scripts.script_params_info', 'action_script_params_info')
            .leftJoinAndSelect(
                'action_script_params_info.script_params_value',
                'action_script_params_value',
                'action_script_params_value.actionId = actions.id')

            .leftJoinAndSelect('actions.command', 'action_commands')
            .leftJoinAndSelect('action_commands.command_actions', 'command_actions')
            .leftJoinAndSelect('command_actions.command_params_info', 'action_command_params_info')
            .leftJoinAndSelect(
                'action_command_params_info.command_params_value',
                'action_command_params_value',
                'action_command_params_value.actionId = actions.id')

            .leftJoinAndSelect('testcases.outputs', 'outputs')
            .leftJoinAndSelect('outputs.script', 'output_scripts')
            .leftJoinAndSelect('output_scripts.script_params_info', 'output_script_params_info')
            .leftJoinAndSelect(
                'output_script_params_info.script_params_value',
                'output_script_params_value',
                'output_script_params_value.outputId = outputs.id')

            .leftJoinAndSelect('testcases.project', 'project')
            .leftJoinAndSelect('testcases.tags', 'tags')

            .orderBy('actions.order', 'ASC')
            .addOrderBy('actions.id', 'ASC')
            .addOrderBy('outputs.order', 'ASC')
            .addOrderBy('outputs.id', 'ASC')
            .addOrderBy('command_actions.order', 'ASC')
            .addOrderBy('action_script_params_info.id', 'ASC')
            .addOrderBy('output_script_params_info.id', 'ASC')
            .where('testcases.id = :id', { id: tcId })
            .getOne();
    }

    // phòng hờ trường hợp script và command được sửa đổi, nhưng chưa được cập nhật bên testcase,
    // điển hình là trường hợp thêm params, nhưng bên testcase chưa lưu, sẽ khiến cho testcase chạy sai.
    async formatReturnData(data) {
        const res = { ...data };

        for (const action of res.actions) {
            // format Command
            if (action.command) {
                for (const command_action of action.command.command_actions) {
                    for (const command_param_info of command_action.command_params_info) {
                        if (command_param_info.command_params_value.length === 0) {
                            let replacedValue;
                            if (command_param_info.param_replace_type === 'replace-all')
                                replacedValue = command_action[command_param_info.attributeTo];
                            else
                                replacedValue = command_param_info.param_default_value;

                            command_param_info.command_params_value = [{
                                id: null,
                                value: replacedValue,
                            }];
                        }
                    }
                }
            }
            // format script
            if (action.script) {
                for (const script_param_info of action.script.script_params_info) {
                    if (script_param_info.script_params_value.length === 0) {
                        script_param_info.script_params_value = [{
                            id: null,
                            value: script_param_info.default_value,
                        }];
                    }
                }
            }
        }

        for (const output of res.outputs) {
            // format script
            if (output.script) {
                for (const script_param_info of output.script.script_params_info) {
                    if (script_param_info.script_params_value.length === 0) {
                        script_param_info.script_params_value = [{
                            id: null,
                            value: script_param_info.default_value,
                        }];
                    }
                }
            }
        }

        return res;
    }

    // data: testcase data
    async formatRunData(data) {
        const res = { ...data };
        const commandActions = [];
        const actions = [];
        // handle action
        for (const action of res.actions) {
            if (action.command !== null) {
                const commandAction = action.command.command_actions.map(item => {
                    item = this.commandActionsService.replaceParamByValue(item);

                    // Limit 100 params per command
                    item.order = Number(action.order) + (Number(item.order) * 0.01);
                    return item;
                });

                for (const subCommandAction of commandAction) {
                    const subAction = await this.directParamsService.replaceParamAlias(subCommandAction);
                    commandActions.push({...subAction});
                }
            } else {
                if (action.script !== null) {
                    for (const scriptParamInfo of action.script.script_params_info) {
                        action.script.content = action.script.content
                            .replace(
                                new RegExp(scriptParamInfo.alias, 'gi'),
                                // tslint:disable-next-line:max-line-length
                                scriptParamInfo.script_params_value.length ? scriptParamInfo.script_params_value[0].value : scriptParamInfo.default_value,
                            );
                    }
                    action.action = 'script';
                    action.element = action.script.content;
                }
                const returnAction = await this.directParamsService.replaceParamAlias(action);
                actions.push({ ...returnAction });
            }
        }

        const newInput = [...actions, ...commandActions];
        res.actions = _.sortBy(newInput, ['order']);
        // handle Output
        for (let output of res.outputs) {
            if (output.script !== null) {
                for (const scriptParamInfo of output.script.script_params_info) {
                    output.script.content = output.script.content.replace(
                        new RegExp(scriptParamInfo.alias, 'gi'),
                        // tslint:disable-next-line:max-line-length
                        scriptParamInfo.script_params_value.length ? scriptParamInfo.script_params_value[0].value : scriptParamInfo.default_value,
                    );
                }
                output.element = output.script.content;
            }
            output = await this.directParamsService.replaceParamAlias(output);
        }
        res.outputs = _.sortBy(res.outputs, ['order']);
        return res;
    }

    async save(data): Promise<any> {
        const testcaseSaved = await this.repository.save(data);
        if (testcaseSaved.order === null) {
            testcaseSaved.order = testcaseSaved.id;
            await this.repository.save(testcaseSaved);
        }

        for (const action of data.actions) {
            await this.actionsService.save(action, testcaseSaved);
        }
        for (const output of data.outputs) {
            await this.outputsService.save(output, testcaseSaved);
        }
        const returnData = await this.findById(testcaseSaved.id);

        return await this.formatReturnData(returnData);
    }

    async getAll(): Promise<any> {
        return this.repository.find();
    }

    async updateProject(data) {
        const testcase = await this.repository.findOne(data.testcase);
        const project = await this.projectService.findOne(data.projectId);

        testcase.project = project;

        return await this.repository.save([testcase]);
    }

    async updateTag(data) {
        const testcase = await this.repository.findOne(data.testcase);
        const tags = await this.tagsService.findByIds(data.tagIds);

        testcase.tags = tags;

        return await this.repository.save([testcase]);
    }

    async prepareDownloadData(ids) {
        const fs = require('fs-extra');
        const filePath = __dirname + './../../../public/testcases.json';
        const data = [];
        for (const tcid of ids) {
            const testcase = await this.findById(tcid);

            data.push(testcase);
        }

        fs.outputFile(filePath, JSON.stringify(data), (err) => {
            if (err !== null) {
                // tslint:disable-next-line:no-console
                console.log(err);
            }
        });
    }

    async importJson(file) {
        const data = file.buffer.toString('utf8');
        for (const testcase of JSON.parse(data)) {
            await this.save(testcase);
        }
    }

    async deleteMulti(ids) {
        for (const id of ids) {
            await this.repository.delete(id);
        }
    }

    async swapOrderTestcase(testcaseIds) {
        const tc1 = await this.repository.findOne(testcaseIds[0]);
        let tc2 = await this.repository.findOne(testcaseIds[1]);

        if (typeof tc1 === 'undefined' || typeof tc2 === 'undefined') {
            return false;
        }

        if (tc1.order === null) {
            tc1.order = tc1.id;
        }

        if (tc2.order === null) {
            tc2.order = tc2.id;
        }

        const temp_order = tc2.order;
        let incomingChangeLocationTC;

        if (tc1.order < tc2.order) {
            // list testcases have order from tc1.order to tc2.order
            incomingChangeLocationTC = await this.repository.createQueryBuilder('testcases')
                .where(`testcases.order >= ${tc1.order} AND testcases.order < ${tc2.order}`)
                .orderBy('testcases.order', 'DESC')
                .getMany();
        } else {
            // list testcases have order from tc2.order to tc1.order
            incomingChangeLocationTC = await this.repository.createQueryBuilder('testcases')
                .where(`testcases.order > ${tc2.order} AND testcases.order <= ${tc1.order}`)
                .orderBy('testcases.order')
                .getMany();
        }

        for (const testcase of incomingChangeLocationTC) {
            tc2.order = testcase.order;
            await this.repository.save(tc2);

            tc2 = testcase;
        }

        tc1.order = temp_order;

        await this.repository.save(tc1);

        return true;
    }
}