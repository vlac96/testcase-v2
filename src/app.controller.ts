import { Get, Controller, Render, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @Render('index')
  root(): string {
    return ;
  }

  // @Post()
  // @MessagePattern({ cmd: 'testCases' })
  // testCases(@Body() body): void{
  //   console.log(body);
  // }
}
